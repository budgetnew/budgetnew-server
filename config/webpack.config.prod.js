const path = require('path');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');


module.exports = {
  entry: './src/index.js',
  output: {
    path: path.join(__dirname, '..', 'dist'),
    // filename: 'index.js',
  },
  plugins: [
    new ProgressBarPlugin(),
  ],
  node: {
    fs: 'empty',
    net: 'empty',
  },
  module: {
    rules: [
      {
        test: /.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        // query: {
        //   presets: ['env'],
        // },
      },
    ],
  },
};
