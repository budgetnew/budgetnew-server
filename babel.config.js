module.exports = function config(api) {
  api.cache(true);

  const presets = [
    '@babel/preset-env',
  ];

  const plugins = [
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-optional-chaining',
    '@babel/plugin-proposal-object-rest-spread',
    '@babel/plugin-transform-runtime',
    '@babel/plugin-transform-async-to-generator',
  ];

  return {
    presets,
    plugins,
  };
};
