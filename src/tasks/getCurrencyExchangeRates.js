import axios from "axios";
import config from '../../config';
import Currency from "../models/Currency";
import CurrencyRate from "../models/CurrencyRate";


async function getRatesForPeriod(startDate, endDate, currencies) {
  try {
    const { data } = await axios.get(config.currencyExchangeRateApiUrl, {
      params: {
        start_at: startDate,
        end_at: endDate,
        symbols: currencies.join(','),
      },
    });
    return data;
  } catch (e) {
    console.error(e.message, e.response.data)
  }
}

function parseRates(rates = {}) {
  const sortedRates = {};
  for (const dateStr in rates) {
    const rate = rates[dateStr];
    const parsedDateNumber = Number(dateStr.replace(/\-/g, ''));
    sortedRates[parsedDateNumber] = { ...rate, dateNum: parsedDateNumber, date: dateStr}
  }

  const res = [];
  const vals = Object.values(sortedRates);
  for(let i = 0; i < vals.length; i++) {
    const rate = vals[i];
    res.push({ ...rate });

    let curDate = rate.dateNum;
    const nextDate = vals[i + 1] && vals[i + 1].dateNum;
    if (curDate + 1 < nextDate) {
      while(curDate + 1 < nextDate) {
        res.push({ ...rate, dateNum: curDate + 1, interpolated: true });
        curDate += 1;
      }
    }
  }

  return res;
}

async function saveRatesToDb(rateData, baseCurrencyCode, currenciesToSave) {
  try {
    const currenciesFromDb = await Currency.findAll();
    const currencies = currenciesFromDb.reduce((acc, c) => {
      const { id, acronym } = c.dataValues;
      acc[acronym] = id;
      return acc;
    }, {});
    const baseCurrencyId = currencies[baseCurrencyCode];

    for(const rate of rateData) {
      for(const currencyCode of currenciesToSave) {
        await CurrencyRate.create({
          baseCurrencyId,
          currencyId: currencies[currencyCode],
          value: rate[currencyCode],
          dateStr: rate.dateNum.toString(),
          date: rate.date,
        });
        process.stdout.write('.');
      }
    }
  } catch (err) {
    console.error(err);
  }
}

async function processRates(dateStart, dateEnd, currencies) {
  const { rates, base } = await getRatesForPeriod(dateStart, dateEnd, currencies);
  console.log(`\nFound ${Object.keys(rates).length} records for date period from ${dateStart} to ${dateEnd}`);
  const res = parseRates(rates);
  console.log(`Saving to DB`);
  await saveRatesToDb(res, base, currencies);
}

async function run() {
  await processRates('2012-01-01', '2012-12-31', ['USD', 'RUB']);
  await processRates('2013-01-01', '2013-12-31', ['USD', 'RUB']);
  await processRates('2014-01-01', '2014-12-31', ['USD', 'RUB']);
  await processRates('2015-01-01', '2015-12-31', ['USD', 'RUB']);
  await processRates('2016-01-01', '2016-12-31', ['USD', 'RUB']);
  await processRates('2017-01-01', '2017-12-31', ['USD', 'RUB']);
  await processRates('2018-01-01', '2018-12-31', ['USD', 'RUB']);
  await processRates('2019-01-01', '2019-12-31', ['USD', 'RUB']);
  await processRates('2020-01-01', '2020-04-12', ['USD', 'RUB']);
}



run()
  .then(() => {
    console.log('\nDone!!!!');
    process.exit();
  });
