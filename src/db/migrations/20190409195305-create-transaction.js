'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER.UNSIGNED,
      },
      accountId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        references: {
          model: 'accounts',
          key: 'id',
        },
      },
      userId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      fromDepositId: {
        type: Sequelize.INTEGER.UNSIGNED,
        references: {
          model: 'deposits',
          key: 'id',
        },
      },
      toDepositId: {
        type: Sequelize.INTEGER.UNSIGNED,
        references: {
          model: 'deposits',
          key: 'id',
        },
      },
      transactionTypeId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        references: {
          model: 'transactionTypes',
          key: 'id',
        },
      },
      categoryId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: true,
        references: {
          model: 'categories',
          key: 'id',
        },
        defaultValue: null,
      },
      valueFrom: {
        type: Sequelize.DECIMAL(20,2),
      },
      valueTo: {
        type: Sequelize.DECIMAL(20,2),
      },
      comment: {
        type: Sequelize.STRING
      },
      hidden: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      transactionDate: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('transactions');
  }
};
