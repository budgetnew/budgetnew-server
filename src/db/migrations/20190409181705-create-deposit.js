'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('deposits', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER.UNSIGNED
      },
      accountId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        references: {
          model: 'accounts',
          key: 'id',
        },
      },
      depositTypeId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        references: {
          model: 'depositTypes',
          key: 'id',
        },
      },
      currencyId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        references: {
          model: 'currencies',
          key: 'id',
        },
      },
      name: {
        type: Sequelize.STRING
      },
      balance: {
        type: Sequelize.DECIMAL(20, 2),
        defaultValue: 0,
      },
      ordering: {
        type: Sequelize.INTEGER,
      },
      disabled: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      hidden: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('deposits');
  }
};
