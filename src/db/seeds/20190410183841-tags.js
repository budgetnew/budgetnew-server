'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('tags', [
      {
        id: 1,
        accountId: 1,
        userId: 1,
        name: 'tag1',
        description: 'my first tag',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: 2,
        accountId: 1,
        userId: 1,
        name: 'tag2',
        description: 'my second tag',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: 3,
        accountId: 1,
        userId: 1,
        name: 'trip',
        description: 'tag for journeys',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('tags', null, {});
  }
};
