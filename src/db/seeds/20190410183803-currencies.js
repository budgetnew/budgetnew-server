'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('currencies', [
      {id: 1, acronym: 'RUR', name: 'russian ruble', ordering: 1, createdAt: new Date(), updatedAt: new Date()},
      {id: 2, acronym: 'USD', name: 'united states dollar', ordering: 2, createdAt: new Date(), updatedAt: new Date()},
      {id: 3, acronym: 'EUR', name: 'euro', ordering: 3, createdAt: new Date(), updatedAt: new Date()}
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('currencies', null, {});
  }
};
