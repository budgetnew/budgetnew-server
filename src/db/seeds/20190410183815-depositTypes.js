'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('depositTypes', [
      {id: 1, name: 'cash', ordering: 1, createdAt: new Date(), updatedAt: new Date()},
      {id: 2, name: 'credit card', ordering: 2, createdAt: new Date(), updatedAt: new Date()},
      {id: 3, name: 'e-money', ordering: 3, createdAt: new Date(), updatedAt: new Date()},
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('depositTypes', null, {});
  }
};
