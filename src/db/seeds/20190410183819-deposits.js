'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('deposits', [
      {
        id: 1,
        accountId: 1,
        depositTypeId: 1,
        currencyId: 1,
        name: 'main card',
        balance: '20345.89',
        ordering: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: 2,
        accountId: 1,
        depositTypeId: 2,
        currencyId: 2,
        name: 'main card2',
        balance: '10123.45',
        ordering: 2,
        hidden: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('deposits', null, {});
  }
};
