'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('categories', [
      {
        id: 1,
        accountId: 1,
        name: 'food',
        transactionTypeId: 1,
        ordering: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: 2,
        accountId: 1,
        name: 'clothes',
        ordering: 2,
        transactionTypeId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('categories', null, {});
  }
};
