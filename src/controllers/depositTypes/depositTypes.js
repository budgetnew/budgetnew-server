import express from 'express';
import DepositType from '../../models/DepositType';


let router = express.Router();

router.get('/', async (req, res) => {
  try {
    const depositTypes = await DepositType.findAll();
    res.json(depositTypes);
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
});

export default router;
