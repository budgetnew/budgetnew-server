import express from 'express';
import Account from '../../models/Account';


let router = express.Router();

router.get('/', async (req, res) => {
  try {
    const accounts = await Account.findAll();
    res.json(accounts);
  } catch(err) {
    console.error(err);
    res.status(500).send(err);
  }
});

export default router;
