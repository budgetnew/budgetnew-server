import express from 'express';
import TransactionType from '../../models/TransactionType';


let router = express.Router();

router.get('/', async (req, res) => {
  try {
    const transactionTypes = await TransactionType.findAll();
    res.json(transactionTypes);
  } catch(err) {
    console.error(err);
    res.status(500).send(err);
  }
});

export default router;
