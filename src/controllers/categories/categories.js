import express from 'express';
import Category from '../../models/Category';


let router = express.Router();

router.get('/', async (req, res) => {
  try {
    const {
      transactionTypeId,
    } = req.query;
    const where = {
      accountId: req.user.accountId,
    };
    if (transactionTypeId) {
      where.transactionTypeId = transactionTypeId;
    }
    const categories = await Category.findAll({ where });
    res.json(categories);
  } catch(err) {
    console.error(err);
    res.status(500).send(err);
  }
});

export default router;
