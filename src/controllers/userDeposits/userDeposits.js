import express from 'express';
import Deposit from '../../models/Deposit';
import UserDeposit from "../../models/UserDeposit";
import dbClient from "../../models/dbClient";


let router = express.Router();

router.post('/', async (req, res) => {
  let { depositListUpdate } = req.body;
  if (!depositListUpdate.length) {
    return res.sendStatus(400);
  }

  dbClient.transaction().then(async t => {
    try {
      for(const depositUpdate of depositListUpdate) {
        const { id, ordering, hidden = false} = depositUpdate;
        if (id) {
          const where = {
            id: req,
            accountId: req.user.accountId,
            userId: req.user.id,
          };
          let userDeposit = await UserDeposit.findOne({
            where,
          });
          const newValues = {
            ordering: ordering || null,
            hidden,
          };
          if (!userDeposit) {
            userDeposit = await Deposit.create({
              ...where,
              ...newValues,
            }, {transaction: t});
          } else {
            await userDeposit.update(newValues, {transaction: t});
          }
          res.json(userDeposit);
        }
      }

      res.json({});
      return t.commit();
    } catch(err) {
      console.error(err);
      res.status(500).send(err);
      return t.rollback();
    }
  });
});

export default router;
