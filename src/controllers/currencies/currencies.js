import express from 'express';
import Currency from '../../models/Currency';


let router = express.Router();

router.get('/', async (req, res) => {
  try {
    const currencies = await Currency.findAll();
    res.json(currencies);
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
});

export default router;
