import express from 'express';
import TagModel from '../../orm/models/TagModel';


let router = express.Router();

router.get('/', (req, res) => {
  TagModel
    .query((qb) => {
      let query = qb.where({ account_id: req.user.account_id });

      if (req.query.name) {
        query = query.andWhere('name', 'like', `%${req.query.name.trim()}%`)
      }

      return query;
    })
    .fetchAll()
    .then(function(data) {
      res.json(data.toJSON());
    })
    .catch(function(err) {
      console.error(err);
      res.status(500).send(err);
    });
});

router.put('/', (req, res) => {
  let {
    name,
    description,
  } = req.body;
  if (!name) {
    return res.status(400).send();
  }
  const objToSave = {
    account_id: req.user.account_id,
    user_id: req.user.id,
    name,
    description,
  };

  TagModel
    .forge(objToSave)
    .save()
    .then(function() {
      res.json({});
    })
    .catch(function(err) {
      console.error(err);
      res.status(500).send(err);
    });
});

router.post('/:tagId', (req, res) => {
  const {
    params: {
      tagId
    },
    body: {
      name,
      description
    }
  } = req;

  if (!tagId || !name) {
    return res.status(400).send();
  }

  const objToUpdate = {
    name,
    description,
  };

  if (description) {
    objToUpdate.description = description;
  }

  new TagModel({ id: tagId })
    .save(objToUpdate)
    .then(function(model) {
      res.json(model);
    })
    .catch(function(err) {
      console.error(err);
      res.status(500).send(err);
    });
});

export default router;
