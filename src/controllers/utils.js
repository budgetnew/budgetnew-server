export function pickFromObject(data, keys) {
  const res = {};
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    if (data.hasOwnProperty(key)) {
      res[key] = data[key]
    }
  }
  return res;
}
