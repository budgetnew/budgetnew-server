import express from 'express';
import moment from 'moment/moment';
import sequelize, { Op } from 'sequelize';
import dbClient from '../../models/dbClient';
import Transaction from '../../models/Transaction';
import TransactionType from '../../models/TransactionType';
import Category from '../../models/Category';
import Deposit from '../../models/Deposit';
import Currency from '../../models/Currency';
// import TagModel from '../../orm/models/TagModel';
// import TransactionTagModel from '../../orm/models/TransactionTagModel';


const RESULTS_LIMIT = 50;
const RESULTS_LIMIT_LARGE = 500;
const TRANSACTION_TYPES = {
  EXPENSE: 1,
  INCOME: 2,
  TRANSFER: 3,
};

let router = express.Router();

router.get('/', async (req, res) => {
  try {
    const {
      transactionTypeId,
      depositId,
      categoryId,
      fromDate,
      toDate,
      groupBy,
    } = req.query;

    const where = {
      accountId: req.user.accountId,
    };
    if (transactionTypeId) {
      where.transactionTypeId = transactionTypeId;
    }
    if (depositId) {
      if (transactionTypeId === 1){
        where.fromDepositId = depositId;
      } else if (transactionTypeId === 2) {
        where.toDepositId = depositId;
      }
    }
    if (categoryId) {
      where.categoryId = categoryId;
    }
    if (fromDate || toDate) {
      where.transactionDate = {};
      if (fromDate) {
        where.transactionDate[Op.gte] = moment(fromDate).format();
      }
      if (toDate) {
        where.transactionDate[Op.lt] = moment(toDate).clone().add(1, 'days').format();
      }
    }

    const queryOptions = {
      where,
      include: [
        Category,
        TransactionType,
        { model: Deposit, as: 'fromDeposit', include: [Currency] },
        { model: Deposit, as: 'toDeposit', include: [Currency] },
      ],
      limit: !fromDate && !toDate ? RESULTS_LIMIT : RESULTS_LIMIT_LARGE,
    };

    if (groupBy) {
      if (groupBy === 'category') {
        queryOptions.attributes = [
          'categoryId',
          [sequelize.fn('SUM', sequelize.col('valueFrom')), 'sumFrom'],
          [sequelize.fn('SUM', sequelize.col('valueTo')), 'sumTo'],
        ];
        queryOptions.group = 'categoryId'
      }
    } else {
      queryOptions.order = [ ['transactionDate', 'desc'], ['createdAt', 'desc'] ];
    }

    const transactions = await Transaction.findAll(queryOptions);
    res.json(transactions);
  } catch(err) {
    console.error(err);
    res.status(500).send(err);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const where = {
      id: req.params.id,
      accountId: req.user.accountId,
    };
    const queryOptions = {
      where,
      include: [
        Category,
        TransactionType,
        { model: Deposit, as: 'fromDeposit', include: [Currency] },
        { model: Deposit, as: 'toDeposit', include: [Currency] },
      ],
    };
    const transaction = await Transaction.findOne(queryOptions);
    res.json(transaction);
  } catch(err) {
    console.error(err);
    res.status(500).send(err);
  }
});

router.post('/', (req, res) => {
  let {
    fromDepositId,
    toDepositId,
    categoryId,
    valueFrom,
    valueTo,
    transactionDate,
    comment,
    transactionTypeId,
    tags = [],
  } = req.body;
  if ((!fromDepositId && !toDepositId) || (!valueFrom && !valueTo) ||
    !transactionTypeId || (transactionTypeId !== 3 && !categoryId) || !transactionDate) {
    return res.status(400).send();
  }
  const objToSave = {
    accountId: req.user.accountId,
    userId: req.user.id,
    transactionTypeId: transactionTypeId,
    categoryId: categoryId,
    transactionDate: moment(transactionDate).format('YYYY-MM-DD HH:mm:ss'),
    comment,
  };
  if ([TRANSACTION_TYPES.EXPENSE, TRANSACTION_TYPES.TRANSFER].includes(transactionTypeId)) {
    objToSave.fromDepositId = fromDepositId || null;
    objToSave.valueFrom = valueFrom || null;
  }
  if ([TRANSACTION_TYPES.INCOME, TRANSACTION_TYPES.TRANSFER].includes(transactionTypeId)) {
    objToSave.toDepositId = toDepositId || null;
    objToSave.valueTo = valueTo || null;
  }

  dbClient.transaction().then(async t => {
    try {
      await Transaction.create(objToSave, {transaction: t});
      if (fromDepositId) {
        const fromDeposit = await Deposit.findByPk(fromDepositId);
        await fromDeposit.decrement('balance', { by: valueFrom, transaction: t });
      }
      if (toDepositId) {
        const toDeposit = await Deposit.findByPk(toDepositId);
        await toDeposit.increment('balance', { by: valueTo, transaction: t });
      }
      // if (tags && tags.length > 0) {
      //   const tagsToCreate = [];
      //   try {
      //     const tagsFound = await TagModel
      //       .query(qb => (
      //         qb
      //           .whereIn('name', tags)
      //           .andWhere({ account_id: req.user.account_id })
      //       ))
      //       .fetchAll()
      //       .then((data) => data.toJSON());
      //
      //     if (tagsFound.length > 0) {
      //       tagsFound.forEach(async (tag) => {
      //         await new TransactionTagModel({
      //           transaction_id: trans.id,
      //           tag_id: tag.id
      //         }).save();
      //       });
      //     }
      //
      //     const tagNamesFound = tagsFound.map(tag => tag.name);
      //     tags.forEach(tagName => {
      //       if (!tagNamesFound.includes(tagName)) {
      //         tagsToCreate.push(tagName)
      //       }
      //     });
      //
      //     if (tagsToCreate.length > 0) {
      //       tagsToCreate.forEach(async (tagNameToCreate) => {
      //         const newTag = await new TagModel({
      //           name: tagNameToCreate,
      //           account_id: req.user.account_id,
      //           user_id: req.user.id,
      //         })
      //           .save();
      //         await new TransactionTagModel({
      //           transaction_id: trans.id,
      //           tag_id: newTag.id
      //         }).save();
      //       })
      //     }
      //   } catch(err) {
      //     console.error(err);
      //     res.status(500).send(err);
      //   }
      // }

      res.json({});
      return t.commit();
    } catch(err) {
      console.error(err);
      res.status(500).send(err);
      return t.rollback();
    }
  });
});

export default router;
