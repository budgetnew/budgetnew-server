import express from 'express';
import { pickFromObject } from '../utils';
import Deposit from '../../models/Deposit';
import DepositType from '../../models/DepositType';
import Currency from '../../models/Currency';
import UserDeposit from '../../models/UserDeposit';


let router = express.Router();

router.get('/', async (req, res) => {
  const where = {
    accountId: req.user.accountId,
  };
  try {
    const deposits = await Deposit.findAll({
      where,
      include: [DepositType, Currency, UserDeposit]
    });
    res.json(deposits);
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
});

router.get('/:id', async (req, res) => {
  const where = {
    id: req.params.id,
    accountId: req.user.accountId,
  };
  try {
    const deposit = await Deposit.findOne({
      where,
      include: [DepositType, Currency]
    });
    res.json(deposit);
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
});

router.post('/', async (req, res) => {
  let {
    depositTypeId,
    currencyId,
    name,
    balance = 0,
  } = req.body;

  if (!depositTypeId || !currencyId || !name) {
    return res.status(400).send();
  }
  const depositObject = {
    accountId: req.user.accountId,
    depositTypeId,
    currencyId,
    name,
    balance,
  };

  try {
    const deposit = await Deposit.create(depositObject);
    res.json(deposit);
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
});

router.patch('/:id', async (req, res) => {
  let {
    depositTypeId,
    currencyId,
    name,
    balance = 0,
  } = req.body;

  if (!depositTypeId || !currencyId || !name) {
    return res.status(400).send();
  }

  const where = {
    id: req.params.id,
    accountId: req.user.accountId,
  };
  try {
    const deposit = await Deposit.findOne({
      where,
      include: [DepositType, Currency]
    });
    if (!deposit) {
      return res.status(404).send();
    }
    await deposit.update({
      depositTypeId,
      currencyId,
      name,
      balance
    });
    res.json(deposit);
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
});

export default router;
