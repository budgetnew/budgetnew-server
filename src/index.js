import express from 'express';
import fs from 'fs';
import http from 'http';
import https from 'https';

import routerMiddlewares from './router/middlewares';
import routes from './router/routes';
import config from '../config';

console.log('Using timezone: ', process.env.TZ);

const app = express();
routerMiddlewares(app);
routes(app);

if (config.useHttps) {
  const privateKey  = fs.readFileSync(config.sslPrivateKeyLocation, 'utf8');
  const certificate = fs.readFileSync(config.sslCertificateLocation, 'utf8');
  const credentials = {key: privateKey, cert: certificate};
  const httpsServer = https.createServer(credentials, app);
  httpsServer.listen(config.port);
  console.log(`https started on port ${config.port}`);
} else {
  const httpServer = http.createServer(app);
  httpServer.listen(config.port);
  console.log(`http started on port ${config.port}`);
}
