import passport from 'passport/lib';
import { OAuth2Client } from 'google-auth-library';
import accounts from '../controllers/accounts/accounts';
import deposits from '../controllers/deposits/deposits';
import userDeposits from '../controllers/userDeposits/userDeposits';
import transactions from '../controllers/transactions/transactions';
import transactionTypes from '../controllers/transactionTypes/transactionTypes';
import categories from '../controllers/categories/categories';
import depositTypes from '../controllers/depositTypes/depositTypes';
import currencies from '../controllers/currencies/currencies';
// import tags from '../tags/tags';
import '../models';
import config from '../../config.json';
import User from "../models/User";
import * as googleAuth from './googleAuth';

const rootPath = '/api/v1';

const isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated())
    return next();
  res.sendStatus(401);
};

export default (app) => {

  app.use(`${rootPath}/accounts`, accounts);
  app.use(`${rootPath}/deposits`, isAuthenticated, deposits);
  app.use(`${rootPath}/userDeposits`, isAuthenticated, userDeposits);
  app.use(`${rootPath}/transactions`, isAuthenticated, transactions);
  app.use(`${rootPath}/transactionTypes`, isAuthenticated, transactionTypes);
  app.use(`${rootPath}/categories`, isAuthenticated, categories);
  app.use(`${rootPath}/depositTypes`, isAuthenticated, depositTypes);
  app.use(`${rootPath}/currencies`, isAuthenticated, currencies);
  // app.use(`${rootPath}/tags`, isAuthenticated, tags);

  app.get('/', function (req, res) {
    res.send('Hello World!');
  });

  app.post(`${rootPath}/authViaGoogle`, async function(req, res) {
    const { token } = req.body;

    const client = new OAuth2Client(config.googleApiClientId);

    try {
      const ticketPayload = await googleAuth.verify(client, token);
      const user = await User.findOne({
        where: { email: ticketPayload.email },
      });
      console.log('user', user);
      if (!user) {
        // TODO: we should register a new user
        return res.send("User not found").status(403);
      }
      req.login(user, function(err) {
        if (err) { return res.send(err).status(500); }
        return res.send('authenticated!');
      });
    } catch (e) {
      console.error(e);
      res.sendStatus(500);
    }
  });

  app.post(
    `${rootPath}/login`,
    passport.authenticate('local'),
    function(req, res) {
      res.json(req.user);
    }
  );

  app.get(
    `${rootPath}/getLoggedIn`, isAuthenticated,
    function(req, res) {
      console.log('getLogged', req.user);
      res.json(req.user);
    }
  );

  app.post(
    `${rootPath}/logout`,
    function(req, res) {
      req.logout();
      res.json(req.user);
    }
  );
};
