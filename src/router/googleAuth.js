import config from "../../config.json";

export async function verify(client, token) {
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: config.googleApiClientId,  // Specify the CLIENT_ID of the app that accesses the backend
    // Or, if multiple clients access the backend:
    //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
  });
  const ticketPayload = ticket.getPayload();
  console.log('payload', ticketPayload);
  // If request specified a G Suite domain:
  //const domain = payload['hd'];
  const {
    aud,
    sub,
    email,
    email_verified,
    given_name,
    family_name,
  } = ticketPayload;
  if (aud !== config.googleApiClientId) {
    throw new Error('Invalid client id');
  }
  if (!email) {
    throw new Error('Invalid email');
  }
  if (!email_verified) {
    throw new Error('Email not verified');
  }
  return ticketPayload;
}
