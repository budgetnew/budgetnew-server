import passport from 'passport/lib';
import passportLocal from 'passport-local/lib';
import { compareSync } from 'bcrypt-nodejs';
import User from '../models/User';


passport.use(new passportLocal.Strategy({
    usernameField: 'email',
    passwordField: 'password'
  },
  async function(username, password, done) {
    console.log(username, password);

    try {
      const user = await User.findOne({
        where: { email: username },
      });
      if (!user) {
        return done(null, false, {message: 'Incorrect username.'});
      }
      const passwordHash = user.password.toString();
      if (!compareSync(password, passwordHash)) {
        return done(null, false, {message: 'Incorrect password.'});
      }
      done(null, user);
    } catch(err) {
      console.error(err);
      done(err);
    }
  }
));

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

export default (app) => {
  app.use(passport.initialize());
  app.use(passport.session());
};
