import sequelize from './dbClient';
import { DataTypes } from 'sequelize';
import Deposit from './Deposit';


const currency = sequelize.define('currency', {
  acronym: DataTypes.STRING,
  name: DataTypes.STRING,
  symbol: DataTypes.STRING,
  ordering: DataTypes.INTEGER,
});

currency.associate = function() {
  currency.hasMany(Deposit);
};

export default currency;
