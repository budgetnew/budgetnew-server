import Sequelize from 'sequelize';


const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../../sequelize.config.json')[env];

let client;
if (config.use_env_variable) {
  client = new Sequelize(process.env[config.use_env_variable], config);
} else {
  client = new Sequelize(config.database, config.username, config.password, config);
}

export default client;
