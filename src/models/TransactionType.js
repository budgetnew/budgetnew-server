import sequelize from './dbClient';
import {DataTypes} from 'sequelize';
import Category from './Category';
import Transaction from './Transaction';


const TransactionType = sequelize.define('transactionType', {
  name: DataTypes.STRING
});

TransactionType.associate = function() {
  TransactionType.hasMany(Category);
  TransactionType.hasMany(Transaction);
};

export default TransactionType;
