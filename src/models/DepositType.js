import sequelize from './dbClient';
import { DataTypes } from 'sequelize';
import Deposit from './Deposit';

const DepositType = sequelize.define('depositType', {
  name: DataTypes.STRING,
  ordering: DataTypes.INTEGER,
});

DepositType.associate = function() {
  DepositType.hasMany(Deposit);
};

export default DepositType;
