import sequelize from './dbClient';
import { DataTypes } from 'sequelize';
import DepositType from './DepositType';
import Currency from './Currency';
import Account from "./Account";
import UserDeposit from "./UserDeposit";


const Deposit = sequelize.define('deposit', {
  accountId: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
    references: {
      model: 'accounts',
      key: 'id',
    },
  },
  depositTypeId: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
    references: {
      model: 'depositTypes',
      key: 'id',
    },
  },
  currencyId: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
    references: {
      model: 'currencies',
      key: 'id',
    },
  },
  name: DataTypes.STRING,
  balance: DataTypes.DECIMAL(20, 2),
  ordering: DataTypes.INTEGER,
  disabled: DataTypes.BOOLEAN,
  hidden: DataTypes.BOOLEAN,
});

Deposit.associate = function() {
  Deposit.belongsTo(Account);
  Deposit.belongsTo(DepositType);
  Deposit.belongsTo(Currency);
  Deposit.hasMany(UserDeposit);
};

export default Deposit;
