import sequelize from './dbClient';
import { DataTypes } from 'sequelize';
import Account from './Account';
import Tag from './Tag';
import Transaction from './Transaction';


const User = sequelize.define('user', {
  login: DataTypes.STRING,
  email: DataTypes.STRING,
  password: DataTypes.STRING.BINARY,
  firstName: DataTypes.STRING,
  lastName: DataTypes.STRING,
  ordering: DataTypes.INTEGER,
  isAdmin: DataTypes.BOOLEAN,
  disabled: DataTypes.BOOLEAN,
});

User.associate = function() {
  User.belongsTo(Account);
  User.hasMany(Tag);
  User.hasMany(Transaction);
};

export default User;
