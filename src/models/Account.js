import sequelize from './dbClient';
import { DataTypes } from 'sequelize';
import User from './User';
import Deposit from './Deposit';
import Tag from './Tag';
import Category from './Category';


const Account = sequelize.define('account', {
  disabled: DataTypes.BOOLEAN
});

Account.associate = function() {
  Account.hasMany(User);
  Account.hasMany(Deposit);
  Account.hasMany(Tag);
  Account.hasMany(Category);
};

export default Account;
