import sequelize from './dbClient';
import {DataTypes} from 'sequelize';
import Account from './Account';
import User from './User';


const Tag = sequelize.define('tag', {
  name: DataTypes.STRING,
  description: DataTypes.STRING,
  hidden: DataTypes.BOOLEAN,
});

Tag.associate = function() {
  Tag.belongsTo(Account);
  Tag.belongsTo(User);
};

export default Tag;
