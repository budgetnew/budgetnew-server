import sequelize from './dbClient';
import {DataTypes} from 'sequelize';
import Account from './Account';
import User from './User';
import Deposit from './Deposit';
import Category from './Category';
import TransactionType from './TransactionType';


const Transaction = sequelize.define('transaction', {
  valueFrom: DataTypes.DECIMAL(20, 2),
  valueTo: DataTypes.DECIMAL(20, 2),
  comment: DataTypes.STRING,
  hidden: DataTypes.BOOLEAN,
  transactionDate: DataTypes.DATE,
});

Transaction.associate = function() {
  Transaction.belongsTo(Account);
  Transaction.belongsTo(User);
  Transaction.belongsTo(Deposit, {
    as: 'fromDeposit',
    sourceKey: 'fromDepositId',
  });
  Transaction.belongsTo(Deposit, {
    as: 'toDeposit',
    sourceKey: 'toDepositId',
  });
  Transaction.belongsTo(Category);
  Transaction.belongsTo(TransactionType);
};

export default Transaction;
