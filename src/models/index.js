import Account from './Account'
import Category from './Category'
import Currency from './Currency';
import Deposit from './Deposit'
import DepositType from './DepositType'
import Tag from './Tag';
import Transaction from './Transaction';
import TransactionTag from './TransactionTag';
import TransactionType from './TransactionType';
import User from './User';


const models = [
  Account,
  Category,
  Currency,
  Deposit,
  DepositType,
  Tag,
  Transaction,
  TransactionTag,
  TransactionType,
  User,
];

models.forEach((model) => {
  if (model.associate) {
    model.associate();
  }
});
