import sequelize from './dbClient';
import { DataTypes } from 'sequelize';
import Account from "./Account";
import User from "./USer";
import Deposit from "./Deposit";


const UserDeposit = sequelize.define('userDeposit', {
  accountId: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
    references: {
      model: 'accounts',
      key: 'id',
    },
  },
  userId: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
    references: {
      model: 'users',
      key: 'id',
    },
  },
  depositId: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
    references: {
      model: 'deposits',
      key: 'id',
    },
  },
  ordering: DataTypes.INTEGER,
  hidden: DataTypes.BOOLEAN,
});

UserDeposit.associate = function() {
  UserDeposit.belongsTo(Account);
  UserDeposit.belongsTo(User);
  UserDeposit.belongsTo(Deposit);
};

export default UserDeposit;
