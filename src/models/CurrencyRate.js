import sequelize from './dbClient';
import { DataTypes } from 'sequelize';
import Currency from './Currency';


const Deposit = sequelize.define('currencyRate', {
  baseCurrencyId: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
    references: {
      model: 'currencies',
      key: 'id',
    },
  },
  currencyId: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
    references: {
      model: 'currencies',
      key: 'id',
    },
  },
  dateStr: DataTypes.STRING,
  date: DataTypes.DATE,
  value: DataTypes.DECIMAL(20, 6),
});

Deposit.associate = function() {
  Deposit.belongsTo(Currency);
};

export default Deposit;
