import sequelize from './dbClient';
import {DataTypes} from 'sequelize';
import Account from './Account';
import TransactionType from './TransactionType';
import Transaction from './Transaction';


const Category = sequelize.define('category', {
  name: DataTypes.STRING,
  ordering: DataTypes.INTEGER,
  disabled: DataTypes.BOOLEAN,
  hidden: DataTypes.BOOLEAN,
});

Category.associate = function() {
  Category.belongsTo(Account);
  Category.belongsTo(TransactionType);
  Category.hasMany(Transaction);
};

export default Category;
